import React from 'react';
import {render} from 'react-dom';
import styled, { createGlobalStyle } from 'styled-components';
import DragMenu from '../../src';

const GlobalStyle = createGlobalStyle`
html{
  box-sizing: border-box;
  margin: 0px;
  padding: 0px;
  display: flex;
  flex-direction: column;
  width: 100%;
  min-height: 100vh;
  overflow: hidden;
}
body{
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  margin: 0px;
  padding: 0px;
  width: 100%;
  min-height: 100%;
  background-color: black;
}
*, *:before, *:after {
    box-sizing: inherit;
    margin: 0px;
    padding: 0px;
}
`
const Container = styled.div`
  position: relative;
  box-sizing: border-box;
  width: 100%;
  height: 100vh;
  margin: 0px;
  padding: 0px;
`
const App = () => {

  return(
    <Container>
      <GlobalStyle />
      <DragMenu></DragMenu>
    </Container>
  ) 
}

render(<App/>, document.querySelector('#demo'))

