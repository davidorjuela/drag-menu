import React from "react";
import {
        MdDashboard,
        MdPlayCircleOutline,
        MdLiveTv,
        MdFiberNew,
        MdStorage,
        MdPodcasts,
        MdOutlineInsights,
        MdOutlineMailOutline,
        MdOutlineMemory,
        MdLogout,
    } from "react-icons/md";


export const linksStore = [
    {
        id: 0,
        name: "Dashboard",
        src: <MdDashboard />,
        to: "/"
    },
    {
        id: 1,
        name: "Videos",
        src: <MdPlayCircleOutline />,
        to: "/videos"
    },
    {
        id: 2,
        name: "Publicidad",
        src: <MdFiberNew />,
        to: "/publicidad"
    },
    {
        id: 3,
        name: "Streaming",
        src: <MdLiveTv />,
        to: "/streaming"
    },
    {
        id: 4,
        name: "Storage",
        src: <MdStorage />,
        to: "/storage"
    },
    {
        id: 5,
        name: "Podcasts",
        src: <MdPodcasts />,
        to: "/podcasts"
    },
    {
        id: 6,
        name: "Mensajes",
        src: <MdOutlineMailOutline />,
        to: "/mensajes"
    },
    {
        id: 7,
        name: "Rendimiento",
        src: <MdOutlineMemory />,
        to: "/rendimiento"
    },
    {
        id: 8,
        name: "Cerrar sesión",
        src: <MdLogout />,
        to: "/cerrar-sesion"
    },
    {
        id: 9,
        name: "Other",
        src: <MdOutlineInsights />,
        to: "/other"
    }
];
