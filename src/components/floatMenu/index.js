import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { linksStore } from './data';
import { AiOutlineSetting, AiOutlineDrag } from 'react-icons/ai';

const Container = styled.div`
	position: fixed;
	left: ${props => `${props.position.left}px`};
	top: ${props => `${props.position.top}px`};
	display: flex;
	flex-direction: ${props => props.menuDirection ? 'row' : 'column'};
	flex-wrap: nowrap;
	justify-content: center;
	align-items: center;
	align-content: center;
	gap: ${props => props.menuDirection ? '.5rem;' : '0rem'};
	box-sizing: border-box;
	width: fit-content;
	height: fit-content;
	${props => props.menuDirection ? 'height: 3rem;' : 'width: 3rem;'};
	margin: 0px;
	padding: ${props => props.menuDirection ? '0px 1rem;' : '1rem 0rem'};
	background: #aaa;
	border-radius: 1rem;
	opacity: 0.3;
	backdrop-filter: blur(2px);
    -webkit-backdrop-filter: blur(2px);
	cursor: pointer;
	z-index: 50;
	transition: opacity .4s ease;
	:hover{
		opacity: .8;
	}
	>svg{
		width: 2rem;
		height: 2rem;
		margin: 0rem;
		padding: .4rem;
	}
	svg:hover{
	color: #5F5;
	}
`;

const QuickLink = styled.div`
position: relative;
top: 0px;//0.25rem;// 0.75rem;
display: flex;
flex-direction: column;
flex-wrap: nowrap;
justify-content: center;
align-content: center;
align-items: center;
margin: 0px;
width: 3rem;
height: 3rem;
background-color: #AAA;
transition: all .3s ease;
span{
	position: absolute;
	bottom: 3px;
	width: 3rem;
	font-size: .8rem;
	color: #5F5;
	font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
	opacity: 0;
	margin: 0px;
	z-index: 52;
	transition: all .3s ease-in-out;
}
svg{
	position: relative;
	top: 0px;
	width: 3rem;
	height: 3rem;
	margin: 0px;
	padding: .5rem;
	border-radius: 1rem;
	background-color: #AAA;
	z-index: 51;
	transition: all .3s ease-in-out;
}
&:hover span {
    opacity: 1;
	text-align: center;
	${props => !props.menuDirection && `
		bottom: .5rem;
		width: 4rem;
	`};
}
&:hover svg {
	color: #5F5;
	top: ${props => props.menuDirection ? '-1rem;' : '-0.5rem;'}
}
&:hover{
	${props => !props.menuDirection && `
	width: 4rem;
	height: 4rem;
	border-radius: 1rem;
	overflow: hidden;
`};
}
`
const SettingsContent = styled.div`
	position: fixed;
	top: 0px;
	left: 0px;
	display: ${props => props.isActive ? 'flex;' : 'none;'};
	flex-direction: column;
	justify-content: center;
	align-items: center;
	align-content: center;
	width: 100%;
	height: 100vh;
	background-color: #333;
	opacity: 0.9;
	z-index: 100;
	user-select: none;
`;

const Settings = styled.div`
	display: flex;
	flex-direction: column;
	width: 100%;
	max-width: 600px;
	height: fit-content;
	background-color: #EEE;
	z-index: 101;
	padding: 2rem;
	border-radius: 1rem;
	box-shadow: .5rem .5rem .5rem .5rem #111;
	font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
	h1{
		font-size: 1.5rem;
		margin: 0rem auto;
	}
	`
const Row = styled.div`
	display: flex;
	flex-wrap: nowrap;
	justify-content: ${props => props.justify};
	align-items: flex-start;
	align-content: center;
	width: 100%;
	border: 1px solid #999;
	padding: 1rem;
	margin-top: 1rem;
	border-radius: 1rem;
	input{
		margin-left: .5rem;
	}
	label{
		font-size: 1rem;
		font-weight: bold;
		line-height: 1.1;
		margin-right: 1.5rem;
	}
`
const RowLink = styled.div`
	display: flex;
	flex-wrap: nowrap;
	justify-content: space-between;
	align-items: center;
	align-content: center;
	width: 100%;
	border: 1px solid #999;
	padding: .5rem;
	margin-bottom: .5rem;
	transition: all .4s ease-in-out;
	input{
		margin-left: .5rem;
	}
	label{
		font-size: 1rem;
		font-weight: bold;
		line-height: 1.1;
		margin-right: 1.5rem;
	}
`

const Links = styled.div`
	display: flex;
	flex-direction: column;
	flex-wrap: nowrap;
	justify-content: flex-start;
	align-items: center;
	justify-content: flex-start;
	box-sizing: border-box;
	width: 50%;
	height: 100%;
	padding: .5rem;
	border: 1px solid #AAA;
	background-color: #FFF;
	label{
		font-size: 1rem;
		font-weight: bold;
		line-height: 1.1;
		margin: 0px 0px .5rem 0px;
	}
`

const Save = styled.button`
	padding: .5rem 1rem;
	margin-left: 1rem;
	border-radius: .5rem;
	border: 1px solid blue;
	background-color: blue;
	color: white;
	transition: all .3s ease;
	:hover{
		background-color: #46F;
		color: white;
		border-color: #46F;
	}
`
const Reset = styled.button`
	padding: .5rem 1rem;
	border-radius: .5rem;
	border: 1px solid blue;
	color: blue;
	background-color: white;
	transition: all .3s ease;
	:hover{
		background-color: #46F;
		color: white;
		border-color: #46F;
	}
`

const App = () => {

	let floatMenu= document.getElementById('floatMenu');
	const [menuSize, setMenuSize] = useState();
	const [position, setPosition] = useState({ top: 0, left: 0 });
	const [isHorizontal, setIsHorizontal] = useState(true);
	const [linksMenu, setLinksMenu] = useState([0,1,2,3,4]);
	const [confTemp, setConfTemp] = useState({ horizontal: true, links: [0,1,2,3,4]} );
	const [settingsView, setSettingsView] = useState(false);

	useEffect(()=>{
		floatMenu = document.getElementById('floatMenu');
		setMenuSize( isHorizontal ? floatMenu.offsetHeight : floatMenu.offsetWidth );
		setPosition({left: (window.innerWidth-floatMenu.offsetWidth)/2, top: window.innerHeight-250 });
	},[]);

	function handleDragStart(e) {
		e.dataTransfer.dropEffect='none';
		e.dataTransfer.effectAllowed='none';
		e.dataTransfer.setData('text/plain', null);
		e.dataTransfer.setDragImage(floatMenu, menuSize/2, menuSize/2);
		e.stopPropagation();
		setTimeout(() => floatMenu.style.display = 'none', 10);
	};

	function handleDragEnd(e) {
		e.stopPropagation();
		e.preventDefault();
		setPosition({ left: (e.clientX - menuSize/2), top: (e.clientY-menuSize/2) });
		setTimeout(() => floatMenu.style.display = 'flex', 0);
	};

	function handleOpenSettings(){
		setConfTemp({links: linksMenu, horizontal: isHorizontal});
		setSettingsView(true);
	}

	function handleChange(state){
		setIsHorizontal(state);
	}

	function handleReset(){
		setLinksMenu(confTemp.links);
		setIsHorizontal(confTemp.horizontal);
	}

	function handleSave(){
		setSettingsView(false);
	}

	function linkDragStart(e) {
		//e.dataTransfer.dropEffect='none';
		//e.dataTransfer.effectAllowed='none';
		e.dataTransfer.setData('text/plain', e.target.id);
		e.stopPropagation();
	};

	function linkDragEnd(e) {
		e.stopPropagation();
		e.preventDefault();
	};

	function handleDrop(e, isUsed){
		e.preventDefault();
		const data = parseInt(e.dataTransfer.getData('text/plain'));
		if(isUsed) {		
			setLinksMenu([...linksMenu, data]);
		}
		else{
			const links = linksMenu.filter((item) => item !== data);
			setLinksMenu(links);
		}
	}

	function handleDragOver(e){
		e.preventDefault();
	}

	function handleDragEnter(e){
		e.preventDefault();
	}

    return(
		<>
		<Container
			id='floatMenu'
			menuDirection={isHorizontal}
			position={position}
			>
			<div 
			draggable={true}
			onDragStart={e=>handleDragStart(e)}
			onDragEnd={e=>handleDragEnd(e)}
			><AiOutlineDrag/></div>
			{linksStore.map((link)=>
				{return linksMenu.includes(link.id) &&
					<QuickLink
						menuDirection={isHorizontal}
						draggable={false}
						key={link.id} 
						to={link.to}
						>
							{link.src}
							<span>{link.name}</span>
					</QuickLink>
				}	
			)}
			<div onClick={e=>handleOpenSettings(e)} >
				<AiOutlineSetting />
			</div>
        </Container>
			<SettingsContent isActive={settingsView} draggable={false}>
				<Settings>
					<h1>Accesos rápidos</h1>
				<Row justify={'flex-start'}>
					<label>Dirección: </label>
					<label>Horizontal
						<input 
							type="radio" 
							name="isHorizontal" 
							checked={isHorizontal}
							onChange={e=>handleChange(true)}
						/>
					</label>
					<label>Vertical
						<input
							type="radio"
							name="isHorizontal"
							checked={!isHorizontal}
							onChange={e=>handleChange(false)}
						/>
					</label>
				</Row>
				<Row justify={'center'}>
					<Links 
						key={'StoreLinks'}
						onDrop={e=>handleDrop(e, false)}
						onDragOver={e=>handleDragOver(e)}
						onDragEnter={e=>handleDragEnter(e)}
						>
						<label>Disponibles</label>
						{linksStore.map((link)=>
							{return !linksMenu.includes(link.id) &&
								<RowLink 
									key={link.id}
									id={link.id}
									draggable={true}
									onDragStart={e=>linkDragStart(e)}
									onDragEnd={e=>linkDragEnd(e)}
									>
									<span>{link.name}</span>
									{link.src}
								</RowLink>
							}	
						)}
					</Links>
					<Links 
						key={'UsedLinks'}
						onDrop={e=>handleDrop(e, true)}
						onDragOver={e=>handleDragOver(e)}
						onDragEnter={e=>handleDragEnter(e)}
						>
						<label>Activos</label>
						{linksStore.map((link)=>
							{return linksMenu.includes(link.id) &&
								<RowLink
									key={link.id}
									id={link.id}
									draggable={true}
									onDragStart={e=>linkDragStart(e)}
									onDragEnd={e=>linkDragEnd(e)}
									>
									{link.src}
									<span>{link.name}</span>
								</RowLink>
							}	
						)}
					</Links>
				</Row>
				<Row justify={'flex-end'}>
					<Reset onClick={e=>handleReset()}>Deshacer cambios</Reset>
					<Save onClick={e=>handleSave()}>Guardar y salir</Save>
				</Row>
				</Settings>
			</SettingsContent>
		</>
    )
}

export default App
