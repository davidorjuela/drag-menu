import React, { useState } from 'react';
import styled from 'styled-components';

const Container = styled.div`
display: flex;
flex-wrap: nowrap;
justify-content: center;
align-items: stretch;
box-sizing: border-box;
align-content: center;
width: 100%;
height: 100vh;
overflow-y: auto;
background: rgb(255,0,35);
background: linear-gradient(135deg, rgba(255,0,35,1) 28%, rgba(12,0,255,1) 69%);
`
const Main = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-content: flex-start;
  background-color: gray;
  box-sizing: border-box;
  width: 100%;
  margin: 0px;
  padding: 2rem;
  gap: 1rem;
  z-index: 5;
`
const LeftMenu = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #AAF;
  box-sizing: border-box;
  margin: 0px;
  padding: 3rem 0px 0px 0px;
  user-select: none;
  overflow: hidden;
  transition: all .25s ease-in-out;
  width: ${props => props.state ? '15rem' : '5rem'};
  cursor: ${props => props.state ? 'w-resize' : 'e-resize'};
`
const RightMenu = styled.div`
  background-color: #AAF;
  box-sizing: border-box;
  margin: 0px;
  padding: 3rem 0px 0px 0px;
  user-select: none;
  transition: all .25s ease-in-out;
  width: ${props => props.state ? '25rem' : '5rem'};
  cursor: ${props => props.state ? 'e-resize' : 'w-resize'};
`
const Card = styled.div`
box-sizing: border-box;
width: 100%;
max-width: 300px;
height: 200px;
border-radius: .5rem;
font-size: 10rem;
background-color: #AAA;
box-shadow: 2px 2px 2px 5px #777;
`
const Row = styled.div`
display: flex;
justify-content: ${props => props.justify};
align-items: center;
align-content: center;
box-sizing: border-box;
height: 2rem;
width: 100%;
padding: 5px;
margin: 1rem 0px;
user-select: none;
cursor: pointer;
overflow: hidden;
`
const Icon = styled.img`
width: 1rem;
height: auto;
margin: 0px 1rem;
transition: all .3s ease-in-out;
user-select: none;
${props => props.rotate && `
    :hover{
      transform: rotate(${props.rotate});
    }`
  }
`
const Text = styled.p`
text-align: center;
font-weight: bolder;
font-size: 1rem;
color: white;
user-select: none;
display: ${props => props.isVisible? 'inline' : 'none'};
`

const canvas = document.createElement("canvas");
canvas.width = canvas.height = 2;

const App = () => {

  const [LMenu, setLMenu] = useState(false);
  const [RMenu, setRMenu] = useState(false);

  const [dragL, setDragL] = useState(0);
  const [dragR, setDragR] = useState(0);

  function handleLDragStart(e, id) {
    e.dataTransfer.effectAllowed = "none";
    e.dataTransfer.setData('text/plain', null);
    e.dataTransfer.setDragImage(canvas, 1, 1);
    console.log(e);
    e.stopPropagation();
    if(id === 'L') setDragL(e.clientX);
    if(id === 'R') setDragR(e.clientX); 
  };

  function handleLDragEnd(e,id) {
    e.stopPropagation();
    e.preventDefault();
    if(id === 'L') dragL > e.clientX ? setLMenu (false) : setLMenu(true);
    if(id === 'R') dragR > e.clientX ? setRMenu (true) : setRMenu(false);
  };

  return(
    <Container>
          <LeftMenu 
              state={LMenu} 
              onDragStart={(e)=>handleLDragStart(e,'L')} 
              onDragEnd={(e)=>handleLDragEnd(e,'L')} 
              draggable="true"
          >
            {[0,1,2,3,4,5].map((v,i)=>
              <Row key={i} justify={'flex-end'}><Text isVisible={LMenu}>AaBbCcDd{v}</Text><Icon draggable={false} rotate={'30deg'} src={'../../src/components/dragMenu/energy.png'}/></Row>
            )}
          </LeftMenu>
          <Main>
            {[0,1,2,3,4,5].map((v,i)=>
              <Card key={i}>{v}</Card>
            )}
          </Main>
          <RightMenu 
              state={RMenu} 
              onDragStart={(e)=>handleLDragStart(e,'R')} 
              onDragEnd={(e)=>handleLDragEnd(e,'R')} 
              draggable="true"
          >
          </RightMenu>
    </Container>
  )
}

export default App
