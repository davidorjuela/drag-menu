import React, { useState } from 'react';
import styled from 'styled-components';
import DragMenu from './components/dragMenu';
import FloatMenu from './components/floatMenu';


const Widescreen = styled.div`
    position: absolute;
    left: ${props => `-${props.des*100}%`};
    display: flex;
    flex-wrap: nowrap;
    justify-content: center;
    align-items: center;
    align-content: center;
    box-sizing: border-box;
    width: 300%;
    height: 100%;
    margin: 0px;
    padding: 0px;
    overflow-y: auto;
    transition: ${props => props.des==1 ? 'none' : 'all 1s ease-in-out'};
`
const Container = styled.div`
    display: flex;
    flex-wrap: nowrap;
    justify-content: center;
    align-items: center;
    align-content: center;
    box-sizing: border-box;
    width: 100%;
    height: 100%;
    overflow-y: auto;
    font-size: 30rem;
    font-weight: bolder;
    color: white;
    background-color: ${props => props.color};
`
const Button = styled.div`
    font-size: 10rem;
    color: #AAA;
    opacity: 0.5;
    cursor: pointer;
    position: fixed;
    line-height: 60%;
    z-index: 10;
    border-radius: 50%;
    overflow: hidden;
    top: calc(50%-5rem);
    ${props => props.left && `left: 20px;`}
    ${props => props.right && `right: 20px;`}
`

const canvas = document.createElement("canvas");
canvas.width = canvas.height = 2;

const App = () => {

    const c1 = <Container color={'red'}    key={1}>1</Container>
    const c2 = <Container color={'orange'} key={2}>2</Container>        
    const c3 = <Container color={'yellow'} key={3}>3</Container>
    const c4 = <Container color={'green'}  key={4}><DragMenu/></Container>
    const c5 = <Container color={'blue'}   key={5}>5</Container>
    const c6 = <Container color={'indigo'} key={6}>6</Container>
    const c7 = <Container color={'violet'} key={7}>7</Container>

    const c = [c1, c2, c3, c4, c5, c6, c7];

    const [visibles, setVisibles] = useState([c.length-1,0,1]);
    const [dragStart, setDragStart] = useState(0);
    const [pos, setPos] = useState(1);
    const [keyPress, setKeyPress] = useState(undefined);
    const [count, setCount] = useState(0);

    function handleLDragStart(e) {
        if(pos===1){
            e.dataTransfer.dropEffect = "none";
            e.dataTransfer.effectAllowed = "none";
            e.dataTransfer.setData('text/plain', null);
            e.dataTransfer.setDragImage(canvas, 0, 0);
            e.stopPropagation();
            setDragStart(e.clientX);
        }else{
            e.preventDefault();
        }
    };

    function handleLDragEnd(e) {

        e.stopPropagation();
        e.preventDefault();

        const sensibility = 50;

        if(Math.abs(dragStart-e.clientX) > sensibility){

            let value = 0;
            if(dragStart < e.clientX) value = -1;
            if(dragStart > e.clientX) value = 1;
            
            runAnimation(value);
        }
    };

    function runAnimation(value){

        if(pos===1){

            setPos(pos+value);

            let sum = [visibles[0]+value, visibles[1]+value, visibles[2]+value];

            const v = sum.map(a=>{
                if(a>=c.length) return (a-c.length);
                else if(a<0) return (a+c.length);
                else return a
            });

            setTimeout(() => {
                setVisibles(v);
                setPos(1);
            }, 999);
        }
    };

    function handleKeyDown (e) {

        const sensibility = 6;
        
        if (e.keyCode === 37 || e.keyCode ===39){

            if(!keyPress) {
                setKeyPress(e.keyCode);
                setCount(0);
            }

            if(keyPress === e.keyCode){

                if(count<sensibility) setCount(count+1);
                else{

                    if (e.keyCode === 37) {runAnimation(-1); setCount(1);}
                    if (e.keyCode === 39) {runAnimation(1); setCount(1);}

                }

            }else{
                setCount(0);
                setKeyPress(e.keyCode);
            }
        }else{
            setCount(0);
            setKeyPress(undefined);
        } 
    };

    function handleKeyUp (e) {
        if (e.keyCode === 37 || e.keyCode ===39){
            setCount(0);
            setKeyPress(undefined);
        }
    };

    //document.addEventListener('keydown',(e)=>getKeyDown(e));
    //document.addEventListener('keyup',(e)=>getKeyUp(e))

    return(
        <Widescreen 
        des={pos}
        tabIndex={'0'}
        onKeyDown = {(e)=>handleKeyDown(e)}
        onKeyUp = {(e)=>handleKeyUp(e)} 
        onDragStart={(e)=>handleLDragStart(e)} 
        onDragEnd={(e)=>handleLDragEnd(e)} 
        draggable="true">
            <FloatMenu/>
            {c[visibles[0]]}
            {c[visibles[1]]}
            {c[visibles[2]]}
            <Button left onClick={() => runAnimation(-1)}>&lt;</Button>
            <Button right onClick={() => runAnimation(1)}>&gt;</Button>
        </Widescreen>
    )
}

export default App
